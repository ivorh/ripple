<?php
/**
 * @file
 * Install, update and uninstall functions for the IH installation profile.
 */

/**
 * Implements hook_install().
 *
 * Perform actions to set up the site for this profile.
 *
 * @see system_install()
 */
function ih_base_install_install() {

  // Users can only be created by administrators.
  variable_set('user_register', USER_REGISTER_ADMINISTRATORS_ONLY);

  // Enable default permissions for system roles.
  user_role_grant_permissions(DRUPAL_ANONYMOUS_RID, array('access content', 'search content'));
  user_role_grant_permissions(DRUPAL_AUTHENTICATED_RID, array('access content', 'search content'));
  
  // Create a default role for site administrators, with all available permissions assigned.
  $admin_role = new stdClass();
  $admin_role->name = 'administrator';
  $admin_role->weight = 2;
  user_role_save($admin_role);
  user_role_grant_permissions($admin_role->rid, array_keys(module_invoke_all('permission')));
  // Set this as the administrator role.
  variable_set('user_admin_role', $admin_role->rid);

  // Create a default editor role with no specific permissions
  $editor_role = new stdClass();
  $editor_role->name = 'editor';
  $editor_role->weight = 3;
  user_role_save($editor_role);

  // Assign user 1 the "administrator" role.
  db_insert('users_roles')
    ->fields(array('uid' => 1, 'rid' => $admin_role->rid))
    ->execute();

  // Enable the admin theme.
  db_update('system')
    ->fields(array('status' => 1))
    ->condition('type', 'theme')
    ->condition('name', 'seven')
    ->execute();
  variable_set('admin_theme', 'seven');
  variable_set('node_admin_theme', '1');
  
  // Default the regional settings to UK and tweak timezone handling
  variable_set('site_default_country', 'GB');
  variable_set('date_first_day', '1');
  variable_set('date_default_timezone', 'Europe/London');
  variable_set('configurable_timezones', '0');
  
  // Set date formats to be more English
  variable_set('date_format_long', 'l, j F Y - g:ia');
  variable_set('date_format_medium', 'j F Y - g:ia');
  variable_set('date_format_short', 'd/m/Y - H:i');
  
  // jQuery update versions
  variable_set('jquery_update_jquery_version', '1.10');
  variable_set('jquery_update_jquery_admin_version', 'default');
  
  // Disable meta tags for User profiles
  variable_set('metatag_enable_user', '0');
  
  // Advanced CSS options
  variable_set('advagg_core_groups', '0');
  variable_set('advagg_ie_css_selector_limiter', '1');
  
  // Increase the GD image quality
  variable_set('image_jpeg_quality', '90');
  
  // Pathauto default settings
  variable_set('pathauto_transliterate', '1');
  variable_set('pathauto_reduce_ascii', '1');
  
  // Menu block settings
  variable_set('menu_block_suppress_core', '1');
  
  // Increase the database logging amount
  variable_set('dblog_row_limit', '10000');
  
  // Set the Menu Position Rules parent behaviour
  variable_set('menu_position_active_link_display', 'parent');
  
  // Set the Views UI to always show master view
  variable_set('views_ui_show_master_display', '1');
  
  // Rebuild permissions
  node_access_rebuild();
}