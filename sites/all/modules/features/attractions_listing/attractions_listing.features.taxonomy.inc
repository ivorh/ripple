<?php
/**
 * @file
 * attractions_listing.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function attractions_listing_taxonomy_default_vocabularies() {
  return array(
    'attraction_type' => array(
      'name' => 'Attraction Type',
      'machine_name' => 'attraction_type',
      'description' => 'Types of attractions',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
    ),
  );
}
