<?php
/**
 * @file
 * attractions_listing.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function attractions_listing_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "context" && $api == "context") {
    return array("version" => "3");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function attractions_listing_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_image_default_styles().
 */
function attractions_listing_image_default_styles() {
  $styles = array();

  // Exported image style: thumbnail_50x50.
  $styles['thumbnail_50x50'] = array(
    'label' => 'Thumbnail 50x50',
    'effects' => array(
      1 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 50,
          'height' => 50,
        ),
        'weight' => 1,
      ),
    ),
  );

  return $styles;
}

/**
 * Implements hook_node_info().
 */
function attractions_listing_node_info() {
  $items = array(
    'attractions' => array(
      'name' => t('Attractions'),
      'base' => 'node_content',
      'description' => t('Content type used to create <i>Attractions</i> content'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'attractions_listing_page' => array(
      'name' => t('Attractions Listing Page'),
      'base' => 'node_content',
      'description' => t('Content type used to create <i>Attractions Listing Page<i> which in turn will display <i>Attractions</i>'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
