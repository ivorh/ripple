<?php
/**
 * @file
 * attractions_listing.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function attractions_listing_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'view_display_attractions_listing';
  $context->description = 'Context which displays the attractions listing view on the attractions listing page';
  $context->tag = 'views';
  $context->conditions = array(
    'entity_field' => array(
      'values' => array(
        'entity_type' => 'a:1:{s:4:"node";s:4:"node";}',
        'field_name' => 'field_category',
        'field_status' => 'all',
        'field_value' => '',
      ),
    ),
    'node' => array(
      'values' => array(
        'attractions_listing_page' => 'attractions_listing_page',
      ),
      'options' => array(
        'node_form' => '1',
      ),
    ),
    'path' => array(
      'values' => array(
        '~node/add/*' => '~node/add/*',
        '~node/edit/*' => '~node/edit/*',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-attractions_list-block' => array(
          'module' => 'views',
          'delta' => 'attractions_list-block',
          'region' => 'content',
          'weight' => '9',
        ),
      ),
    ),
  );
  $context->condition_mode = 1;

  // Translatables
  // Included for use with string extractors like potx.
  t('Context which displays the attractions listing view on the attractions listing page');
  t('views');
  $export['view_display_attractions_listing'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'view_display_attractions_listing_exposed';
  $context->description = '';
  $context->tag = 'views';
  $context->conditions = array(
    'entity_field' => array(
      'values' => array(
        'entity_type' => 'a:1:{s:4:"node";s:4:"node";}',
        'field_name' => 'field_category',
        'field_status' => 'empty',
        'field_value' => '1',
      ),
    ),
    'node' => array(
      'values' => array(
        'attractions_listing_page' => 'attractions_listing_page',
      ),
      'options' => array(
        'node_form' => '1',
      ),
    ),
    'path' => array(
      'values' => array(
        '~node/add/' => '~node/add/',
        '~node/edit/' => '~node/edit/',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-attractions_list-block_1' => array(
          'module' => 'views',
          'delta' => 'attractions_list-block_1',
          'region' => 'content',
          'weight' => '9',
        ),
      ),
    ),
  );
  $context->condition_mode = 1;

  // Translatables
  // Included for use with string extractors like potx.
  t('views');
  $export['view_display_attractions_listing_exposed'] = $context;

  return $export;
}
